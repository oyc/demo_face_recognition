[![pipeline status](https://gitlab.com/oyc/demo_face_recognition/badges/master/pipeline.svg)](https://gitlab.com/oyc/demo_face_recognition/commits/master)
[![coverage report](https://gitlab.com/oyc/demo_face_recognition/badges/master/coverage.svg)](https://gitlab.com/oyc/demo_face_recognition/commits/master)

# demo_face_recognition


Table of Content:

* [Getting started with Ubuntu/Debian](#getting_started_deb)
  * [Prerequisites](#prerequisites)
  * [Installing](#install)
  * [Start the application](#start_app)
* [Getting started with Ansible](#getting_started_ansible)
  * [Install on local host](#ansible_local_host)
  * [Install on remote host](#ansible_remote_host)
* [Getting started with Docker](#getting_started_docker)
  * [Install Docker](#install_docker)
  * [Clone repository](#clone_repository)
  * [Build container](#build_containers)
  * [Deploy container](#deploy_container)
* [Update the photo database](#database_update)
* [Running the tests](#tests)
* [Coding style tests](#style_tests)
* [Deployment](#deployment)
* [Contributing](#contributing)


## Getting Started with Ubuntu/Debian
<a name="getting_started_deb"></a>


#### 1) Prerequisites
<a name="prerequisites"></a>

A large number of apt packages needs to be installed in order to later install
`demo_face_recognition`. You may build as much as those dependencies manually
but this example shows how to install them woth `apt-get`:

```bash
sudo apt update
sudo apt install -y build-essential \
    cmake \
    gfortran \
    git \
    wget \
    curl \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libatlas-dev \
    libatlas-base-dev \
    liblapack-dev \
    libatlas3-base \
    libavcodec-dev \
    libavformat-dev \
    libboost-all-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    libilmbase-dev \
    libopenexr-dev \
    libswscale-dev \
    libjasper-dev \
    pkg-config \
    python3-pip \
    python3-dev \
    python3-numpy \
    zip \
    libopenblas-dev \
    qt5-default \
    pyqt5-dev \
    pyqt5-dev-tools \
    libqt4-test \
    libqtgui4 \
    libportaudio-dev \
    python3-pyaudio
```

Once those dependencies are installed, add your user to the `video` and `audio`
 group:

```bash
sudo usermod -aG video <username>
sudo usermod -aG audio <usermod>
```

This will allow you to access audio and video drivers.

#### 2) Installing
<a name="install"></a>

It is easier to install the python packages inside a virtual environment, but
since we installed some dependencies globally with `apt`, we need to give the
virtual environment access to the system site packages.

```bash
virtualenv --system-site-packages ~/.venv_demo_face_recognition
```

Once the virtual environment is installed, activate it and use `pip` to install
 `demo_face_recognition`:

```bash
source ~/.venv_demo_face_recognition/bin/activate
pip install git+https://gitlab.com:oyc/demo_face_recognition.git
```

#### 3) Start the application
<a name="start_app"></a>

You can set your MIC name through environment variable before starting the app:

```bash
export MIC_NAME="Jabra UC VOICE 550a MS: USB Audio (hw:2,0)"
```

If the variable is not set it will default to "HD Webcam C615: USB Audio (hw:1,0)".

If you want to detect the name of your microphone, type `alsamixer`, press F6,
and take a look at the names displayed. You should be able to find the one you want.

Else you could youse `pyaudio` package which is installed in the virtualenv:

```python
import pyaudio

p = pyaudio.PyAudio()
for idx in range(p.get_device_count()):
  device_info = p.get_device_info_by_index(idx)
  print("#### Discovered new device: {0}\n".format(device_info["name"]))
  print(device_info)
  print("\n")
```


You then have to run the following command to start the application:

```bash
demo_face_recognition
```

> Note: The program will fail if either:
> - no microphone is detected
> - no camera is detected.


## Getting started with Ansible
<a name="getting_started_ansible"></a>

Two playbooks are available in order to install `demo_face_recognition` with
ansible:

- [install_face_recognition_on_rpiARMV7.yml](https://gitlab.com/oyc/demo_face_recognition/blob/master/ansible/install_face_recognition_on_rpiARMV7.yml): Install the demo on Raspberry pi with latest debian stretch.

  This playbook has been tested on a RPI 3B+ after a fresh install and everything
  does install.

- [install_face_recognition_on_ubuntu.yml](https://gitlab.com/oyc/demo_face_recognition/blob/master/ansible/install_face_recognition_on_ubuntu.yml): Install the demo on Ubuntu Xenial 16.04 or Debian Stretch.

  This has not been tested yet.

You can configure the user you want to create and the password you want to set
in the [ansible/group_vars/face_recognition.yml file](https://gitlab.com/oyc/demo_face_recognition/blob/master/ansible/group_vars/face_recognition.yml)

> Note: The user you create is the user that will start the application.
 This is not the user who ansible uses to connect. If you want to change the user
 that is used to connect, look at the inventories files.

> Note: You have to be in the `ansible` directory to run the playbook.

<a name="ansible_local_host"></a>
- You can run the following command to install on your local host Ubuntu:

  ```bash
  ansible-playbook -i inventories/localhost install_face_recognition_on_ubuntu.yml
  ```
  In this case the file `inventories/localhost` is read as inventory.
  You can modify it to use another user than root to connect with ansible.

<a name="ansible_remote_host"></a>
- or install on a remote raspberry pi:

  ```bash
  ansible-playbook -i inventories/raspberry install_face_recognition_on_rpiARMV7.yml
  ```
  In this case the file `inventories/raspberry` is read as inventory.
  You can modify it to match the IP of your raspberry.

> Note:
> Ansible needs to be present on the controler host. I recommend installing
> ansible inside a virtual environment in order not to mess with system version of
> `cryptography` package. (You will surely have an error.)
> Use the following command to install ansible with virtualenv.

```bash
pip install --user virtualenv
virtualenv ~/.ansible_venv
source ~/.ansbile_venv
pip install ansible==2.7
```


## Getting Started with Docker
<a name="getting_started_docker"></a>



#### 1) Install docker
<a name="install_docker"></a>

  - For ubuntu users, see [Get Docker Community Edition for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/) or simply run :
    ```
    curl -fsSL get.docker.com -o get-docker.sh
    sh get-docker.sh
    ```
    It will install Docker and set up your apt repository accordingly.

    > For a quick setup this is the prefered way, but for a stable installation,
    it is prefered to install Docker the way it is described in the Official Documentation.
    Sometimes the script can lead to errors.

##### 1.1) Clone repository
<a name="clone_repository"></a>

Use Git to clone repository on your local machine. (`sudo apt install -y git`)

```bash
git clone https://gitlab.com:oyc/demo_face_recognition.git
```


##### 1.2) Build containers
<a name="build_containers"></a>

Use docker to build images:

```bash
docker build -t demo_face_recognition:test docker/.
```

It will build the following image:

  * **demo_face_recognition:test**


##### 1.3) Deploy container
<a name="deploy_container_and_run"></a>

Use Docker to deploy the container:

```bash
docker run -d
  --user $(id -u) \
  -e DISPLAY=unix$DISPLAY \
  --workdir=$(pwd) \
  --volume="/home/$USER:/home/$USER" \
  --volume="/etc/group:/etc/group:ro" \
  --volume="/etc/passwd:/etc/passwd:ro" \
  --volume="/etc/shadow:/etc/shadow:ro" \
  --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  demo_face_recognition:test
```


## Updating the photos database
<a name="database_update"></a>

If you want to add your own photo to the database, source the virtualenv
environment, and use the dedicated command:

```bash
source ~/.venv_demo_face_recognition
update_demo_database --input-dir="~/Pictures" --pattern="*.jpg"
```

This will copy all files ending with `.jpg` inside the demo database.

## Running the tests
<a name="tests"></a>

Unit tests are written using [unittest](https://docs.python.org/3.6/library/unittest.html) library.

All unit tests are available in folder [demo_face_recognition/tests](https://gitlab.com/oyc/demo_face_recognition/tree/master/demo_pf/tests)

Tests are ran with GitLab CI/CD each time a commit is pushed to the repository.

Integration tests can be written directly inside `.gitlab-ci.yml`

See last pipeline executions: https://gitlab.com/oyc/demo_face_recognition/pipelines

![Example of CI pipeline](https://image.ibb.co/d45inK/demo_pipeline.png)


## Coding style tests
<a name="style_tests"></a>

We use [pylint](https://www.pylint.org) to check that coding style is OK. From the root of the repository simply run:

```bash
pylint demo_pf/.
```
If `pylint` is not installed, install it with pip:

```bash
pip install pylint
```

A job is defined in `.gitlab-ci.yml` to run pylint in GitLab!


## Contributing
<a name="contributing"></a>

Please read [CONTRIBUTING.md](https://gitlab.com/oyc/demo_face_recognition/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning
<a name="versioning"></a>



## Authors
<a name="authors"></a>

**Author**: Guillaume Charbonnier

See the list of [contributors](https://gitlab.com/oyc/demo_face_recognition/graphs/master) who participated in this project.

## License



## Acknowledgments

* This demo would not have existed without [ageitgey/face_recognition](https://github.com/ageitgey/face_recognition)
