"""
Resources module of demo_face_recognition package
"""

import os

__PACKAGE_DIR__ = os.path.dirname(__file__)

_LOGOS_PATH = os.path.join(__PACKAGE_DIR__, "logos")
_BADGES_PATH = os.path.join(__PACKAGE_DIR__, "badges")
_DATABASE_PATH= os.path.join(__PACKAGE_DIR__, "database")
_LISTS_PATH = os.path.join(__PACKAGE_DIR__, "lists")

def get_logo_path(img):
    return os.path.join(_LOGOS_PATH, img)

def get_badge_path(name):
    filename = name + "jpg"
    return os.path.join(_BADGES_PATH, filename)

def get_database_path():
    return _DATABASE_PATH

def get_guest_list():
    return os.path.join(_LISTS_PATH, "demo_list.txt")
