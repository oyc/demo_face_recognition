"""
Unit test file for package entrypoints
"""

import unittest
import subprocess

class TestDummy(unittest.TestCase):

    def tearDown(self):
    # This is executed ater each test function evaluation
    # You can for example remove files, or clean folder.
    # We do not break things as the default implementation does nothing
    # See https://docs.python.org/3.6/library/unittest.html for complete documentation
        pass

    def test_first_entrypoint(self):
    # You can use subprocess.Popen to start entrypoint the same way you would in a Docker container.
    # Use `stdout = subprocess.PIPE` with function subprocess.Popen() to catch the output of the process
    # Same apply to stderr argument: `stderr = subprocess.PIPE`
    # See https://stackoverflow.com/questions/13332268/python-subprocess-command-with-pipe?answertab=votes#tab-top
        assert True

if __name__ == '__main__':
    unittest.main()
