"""
Person module of demo_face_recognition package.

This module defines a class named Person which is used to store people \
as objects
"""

from pathlib import Path

import face_recognition
import cv2

from demo_face_recognition.voice_interaction import Speak
from demo_face_recognition.badge_generator import BadgeGenerator

class Person:
    def __init__(self, name, company, function, speech, database_folder):
        self.name = name
        self.company = company
        self.function = function
        self.picture_file = database_folder + self.name + ".jpg"
        picture_file_path = Path(self.picture_file)
        if picture_file_path.is_file():
            image = face_recognition.load_image_file(self.picture_file)
            self.face = face_recognition.face_encodings(image)[0]
        else:
            self.face = None

        self.speech = speech
        self.welcomed = False
        self.waiting_to_confirm = 0
        self.is_here = False
