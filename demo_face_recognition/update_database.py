"""
update_database module of `demo_face_recognition` package.

You will find in this module a function named update_database which is \
defined in entrypoints in setup.py as update_demo_database.
"""
import os
from shutil import copy
import argparse
from glob import glob

from demo_face_recognition.resources import _DATABASE_PATH


def add_files():
    """
    Command line utility to add files inside the data bank (folder database)
    """

    parser = argparse.ArgumentParser()


    parser.add_argument('--input-dir',
                        metavar="INPUT_DIRECTORY",
                        required=True,
                        type=str,
                        help="Path to the input directory")


    parser.add_argument('--pattern',
                        metavar="GLOB_PATTERN",
                        required=False,
                        default="*.jpg",
                        type=str,
                        help="Pattern of files to look for. Default to '*.jpg'"
                        )
    # Not implemented yet
    # parser.add_argument('--overwrite',
    #                     action=store_true,
    #                     help="If set existing files will be overwritten")


    args = parser.parse_args()

    search_args = [args.input_dir, args.pattern]
    print("Scanning directory {0} for files ending with {1}".format(*search_args))
    search_path = os.path.join(*search_args)
    files_to_copy = glob(search_path)

    for file in  files_to_copy:
        print("Copying {0} into directory {1}".format(file, _DATABASE_PATH))
        copy(file, _DATABASE_PATH)
