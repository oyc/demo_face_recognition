# -*- python -*-

import setuptools
from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()

def requirements():
    with open('requirements.txt') as f:
        return [req.strip() for req in f.readlines()]
# Please read https://docs.python.org/3.6/distutils/setupscript.html for better understanding of Python packaging.

setup(name="demo_face_recognition",
    version="0.1",
    description="Demo face recognition application",
long_description=readme(),
    classifiers=[
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='',
    url="https://gitlab.com/oyc/demo_face_recognition",
    author="Guillaume Charbonnier",
    author_email="guillaume.charbonnier@capgemini.com",
    license='',
    packages=find_packages(include=["demo_face_recognition"]),
    test_suite = 'nose.collector',
    tests_require = ['nose'],
    zip_safe=False,
   install_requires=requirements(),
    scripts=[],  #Put the scripts placed in /bin folder here so that they can be used directly from command line.
    entry_points={
        'console_scripts': [
            'demo_face_recognition = demo_face_recognition.cli:main',
            'update_demo_database = demo_face_recognition.update_database:add_files'
        ],
    },
    include_package_data=True
    )
